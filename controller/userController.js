const client = require('../config/connection')
const moment = require('moment')

// get users
async function getUser (req, res){
    try{
        const responseData = await client.query('select * from users')
        res.status(200).json({
            'data': responseData.rows
        })
    } catch (err) {
        console.log(err.message)
    }
}

// create user
async function createUser (req, res){
    try{
        const {name} = req.body
        const responseData = await client.query(`insert into users(name, createdat) values('${name}', CURRENT_TIMESTAMP)`)
        res.status(200)
        res.send('Insert Sucsess')
    } catch (err) {
        console.log(err.message)
    }
}

// get product by id user
async function getProductByIdUser (req, res) {
    try {
        const responseData = await client.query('select * from users join books on users.id = books.user_id')
        res.status(200).json({
            'data': responseData.rows
        })
    } catch (err) {
        
    }
}

module.exports = {
    getUser,
    createUser,
    getProductByIdUser
}
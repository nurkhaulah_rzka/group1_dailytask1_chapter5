const bodyParser = require("body-parser");
const express = require("express");
const morgan = require('morgan');
const path = require('path');

// our own module
const routes = require('./routes/index')

const PORT = process.env.PORT || 8000;

// Intializations
const app = express();
const client = require('./config/connection.js')

// Settings view engine
app.set("views", __dirname + "/views");
app.set("view engine", "ejs");

// Set JSON
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
// app.use(express.json())

// Public
app.use(express.static(path.join(__dirname, "public")));
app.use(express.static(path.join(__dirname, "controller")));

// third party middleware
app.use(morgan('dev'))

// connect to db
client.connect(err => {
    if (err) {
        console.log(err.message)
    } else {
        console.log('Connected')
    }
})

// routes
app.use(routes)

// app.get('/', (req, res) => {
//     let productsData = client.query('select * from books')
//     res.render("index", {
//         productsData,
//     });
// })
// Halaman Detail Testing
// app.get('/detail', (req, res) => {
//     res.render('detail')
// })
// server run
app.listen(PORT, () => {
    console.log(`Express nyala di http://localhost:${PORT}`);
});

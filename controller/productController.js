const client = require('../config/connection')
const moment = require('moment')

// get data
async function getProduct(req, res) {
    try {
        const responseData = await client.query('select * from books')
        res.status(200).json({
            'data': responseData.rows
        })
    } catch (err) {
        console.log(err.message)
    }
}

async function getProductById(req, res) {
    try {
        const id = req.params.id
        const responseData = await client.query(`select * from books where id = '${id}'`)
        res.status(200).json({
            'status': 'success',
            'data': responseData.rows
        })  

    } catch (err) {
        console.log(err.message)
    }
}

// create data
async function createProduct(req, res) {
    try {
        const { title_book, writer, year_publish, user_id } = req.body
        const responseData = await client.query((`insert into books(title_book, writer, year_publish, user_id, createdat) values('${title_book}', '${writer}', '${year_publish}', '${user_id}', CURRENT_TIMESTAMP)`))
        res.status(200)
        res.send('Insert Sucsess')
    } catch (err) {
        console.log(err.message)
    }
}


// update data
async function updateProduct(req, res) {
    try {
        const { title_book, writer, year_publish, user_id } = req.body
        const id = req.params.id
        const responseData = await client.query((`update books set title_book = '${title_book}', writer = '${writer}', year_publish = '${year_publish}', user_id = '${user_id}', createdat = CURRENT_TIMESTAMP where id = '${id}'`))
        res.status(200)
        res.send('Update Sucsess')
    } catch (err) {
        console.log(err.message)
    }
}

// delete data
async function deleteProduct(req, res) {
    try {
        const id = req.params.id
        const responseData = await client.query(`delete from books where id = '${id}'`)
        res.status(200)
        res.send('Delete Sucsess')
    } catch (err) {
        console.log(err.message)
    }
}

module.exports = {
    getProduct,
    createProduct,
    updateProduct,
    deleteProduct,
    getProductById
}
const router = require("express").Router();

const productController = require('../controller/productController')
const userController = require('../controller/userController')
const adminProductController = require('../controller/admin/productController')

// API Product
router.get('/api/products', productController.getProduct)
router.post('/api/products', productController.createProduct)
router.get('/api/products/:id', productController.getProductById)
router.put('/api/products/:id', productController.updateProduct)
router.delete('/api/products/:id', productController.deleteProduct)

// API Users
router.get('/api/users', userController.getUser)
router.post('/api/users', userController.createUser)

// API get books by id user
router.get('/api/getProductByIdUser', userController.getProductByIdUser)

//dashboard admin
router.get('/', adminProductController.getProducts)
router.get('/get', adminProductController.getProductsByIdUser)
router.get('/products', adminProductController.createProductPage)
router.post('/products', adminProductController.createProduct)
router.get('/delete/:id', adminProductController.deleteProduct)
router.get('/product/detail/:id', adminProductController.detailPage)
router.get('/product/edit/:id', adminProductController.editPage)
router.post('/product/edit/:id', adminProductController.editData)

module.exports = router
const client = require('../../config/connection')

// get data
async function getProducts(req, res) {
    const productsData = await client.query('select * from books')
    res.render("index", {
        data: productsData.rows,
        user: 'Semua User'
    });
};

// get product by user id
async function getProductsByIdUser(req, res) {
    const id = req.query.userid
    const productsData = await client.query(`select * from users join books on users.id = books.user_id where users.id = '${id}'`)

    res.render("index", {
        data: productsData.rows,
        user: "name"
    });
};

// GET /books/create
async function createProductPage(req, res) {
    res.render("add");
}

// Post atau Create Product 
async function createProduct(req, res) {
    const { title_book, writer, year_publish, user_id } = req.body
    const responseData = await client.query((`insert into books(title_book, writer, year_publish, user_id, createdat) values('${title_book}', '${writer}', '${year_publish}', '${user_id}', CURRENT_TIMESTAMP)`))
    res.redirect("/");
}

// detail page
async function detailPage(req, res) {
    const id = req.params.id
    const productData = await client.query(`select * from books where id = '${id}'`)
    res.render("detail", {
        data : productData.rows
    });
}

// deleteData
async function deleteProduct(req, res) {
    const id = req.params.id
    await client.query(`delete from books where id = ${id}`)
    res.redirect('/')
}

// edit page
async function editPage(req, res) {
    const id = req.params.id
    const productData = await client.query(`select * from books where id = '${id}'`)
    res.render("edit", {
        data : productData.rows
    });
}

// edit data
async function editData(req, res) {
    const { title_book, writer, year_publish, user_id } = req.body
        const id = req.params.id
        const responseData = await client.query((`update books set title_book = '${title_book}', writer = '${writer}', year_publish = '${year_publish}', user_id = '${user_id}', createdat = CURRENT_TIMESTAMP where id = '${id}'`))
    res.redirect("/");
}

module.exports = {
    getProducts,
    getProductsByIdUser,
    createProduct,
    createProductPage,
    detailPage,
    deleteProduct,
    editPage,
    editData
}